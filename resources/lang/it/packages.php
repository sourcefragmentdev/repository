<?php
return [
    'Sourcefragment_laravel_validation_required' => "Pacchetto richiesto. Installa Sourcefragment/laravel-validation ('composer require Sourcefragment/laravel-validation'), per favore.",
    'league_fractal_required'             => "Pacchetto richiesto. Installa league/fractal ('composer require league/fractal') (0.12.*), per favore."
];
