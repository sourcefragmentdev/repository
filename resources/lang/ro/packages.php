<?php
return [
    'Sourcefragment_laravel_validation_required' => "Pachet obligatoriu. Instalaţi Sourcefragment/laravel-validation ('composer require Sourcefragment/laravel-validation'), vă rog.",
    'league_fractal_required'             => "Pachet obligatoriu. Instalaţi league/fractal ('composer require league/fractal') (0.12.*), vă rog."
];
