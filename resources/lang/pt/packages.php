<?php
return [
    'Sourcefragment_laravel_validation_required' => "Componente requerido. Por favor instale a dependência usando o comando: 'composer require Sourcefragment/laravel-validation'",
    'league_fractal_required'             => "Componente requerido. Por favor instale a dependência usando o comando:  'composer require league/fractal' (0.12.*)"
];
