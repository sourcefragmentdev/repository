<?php

namespace Sourcefragment\LaravelRepository\Traits;

/**
 * Class TransformableTrait
 * @package Sourcefragment\LaravelRepository\Traits
 * @author Krutarth Patel <krutrth@sourcefragment.com>
 */
trait TransformableTrait
{
    /**
     * @return array
     */
    public function transform()
    {
        return $this->toArray();
    }
}
