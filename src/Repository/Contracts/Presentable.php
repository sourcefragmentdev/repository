<?php
namespace Sourcefragment\LaravelRepository\Contracts;

/**
 * Interface Presentable
 *
 * @package Sourcefragment\LaravelRepository\Contracts
 * @author Krutarth Patel <krutarth@sourcefragment.com>
 */
interface Presentable
{
    /**
     * @param PresenterInterface $presenter
     *
     * @return mixed
     */
    public function setPresenter(PresenterInterface $presenter);

    /**
     * @return mixed
     */
    public function presenter();
}