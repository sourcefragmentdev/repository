<?php
namespace Sourcefragment\LaravelRepository\Contracts;

/**
 * Interface CriteriaInterface
 *
 * @package Sourcefragment\LaravelRepository\Contracts
 * @author Krutarth Patel <krutarth@sourcefragment.com>
 */

interface CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository);
}