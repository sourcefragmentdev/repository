<?php
namespace Sourcefragment\LaravelRepository\Contracts;

/**
 * Interface PresenterInterface
 *
 * @package Sourcefragment\LaravelRepository\Contracts
 * @author Krutarth Patel <krutarth@sourcefragment.com>
 */
interface PresenterInterface
{
    /**
     * Prepare data to present
     *
     * @param $data
     *
     * @return mixed
     */
    public function present($data);
}