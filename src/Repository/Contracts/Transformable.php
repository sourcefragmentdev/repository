<?php
namespace Sourcefragment\LaravelRepository\Contracts;

/**
 * Interface Transformable
 *
 * @package Sourcefragment\LaravelRepository\Contracts
 * @author Krutarth Patel <krutarth@sourcefragment.com>
 */
interface Transformable
{
    /**
     * @return array
     */
    public function transform();
}