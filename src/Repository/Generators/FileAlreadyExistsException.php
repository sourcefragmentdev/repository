<?php
namespace Sourcefragment\LaravelRepository\Generators;

use Exception;

/**
 * Class FileAlreadyExistsException
 * @package Sourcefragment\LaravelRepository\Generators
 * @author Krutarth Patel <krutrth@sourcefragment.com>
 */
class FileAlreadyExistsException extends Exception
{
}
