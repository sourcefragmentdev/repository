<?php namespace Sourcefragment\LaravelRepository\Transformer;

use League\Fractal\TransformerAbstract;
use Sourcefragment\LaravelRepository\Contracts\Transformable;

/**
 * Class ModelTransformer
 * @package Sourcefragment\LaravelRepository\Transformer
 * @author Krutarth Patel <krutrth@sourcefragment.com>
 */
class ModelTransformer extends TransformerAbstract
{
    public function transform(Transformable $model)
    {
        return $model->transform();
    }
}
