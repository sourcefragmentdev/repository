<?php
namespace Sourcefragment\LaravelRepository\Presenter;

use Exception;
use Sourcefragment\LaravelRepository\Transformer\ModelTransformer;

/**
 * Class ModelFractalPresenter
 * @package Sourcefragment\LaravelRepository\Presenter
 * @author Krutarth Patel <krutrth@sourcefragment.com>
 */
class ModelFractalPresenter extends FractalPresenter
{

    /**
     * Transformer
     *
     * @return ModelTransformer
     * @throws Exception
     */
    public function getTransformer()
    {
        if (!class_exists('League\Fractal\Manager')) {
            throw new Exception("Package required. Please install: 'composer require league/fractal' (0.12.*)");
        }

        return new ModelTransformer();
    }
}
