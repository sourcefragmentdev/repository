<?php
namespace Sourcefragment\LaravelRepository\Exceptions;

use Exception;

/**
 * Class RepositoryException
 * @package Sourcefragment\LaravelRepository\Exception
 * @author Krutarth Patel <krutarth@sourcefragment.com>
 */
class RepositoryException extends Exception
{

}