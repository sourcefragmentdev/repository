<?php
namespace Sourcefragment\LaravelRepository\Events;

/**
 * Class RepositoryEntityCreated
 *
 * @package Sourcefragment\LaravelRepository\Event
 * @author Krutarth Patel <krutarth@sourcefragment.com>
 */
class RepositoryEntityCreated extends RepositoryEventBase
{
    /**
     * @var string
     */
    protected $action = "created";
}