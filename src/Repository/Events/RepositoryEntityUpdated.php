<?php
namespace Sourcefragment\LaravelRepository\Events;

/**
 * Class RepositoryEntityUpdated
 * @package Sourcefragment\LaravelRepository\Event
 * @author Krutarth Patel <krutarth@sourcefragment.com>
 */
class RepositoryEntityUpdated extends RepositoryEventBase
{
    /**
     * @var string
     */
    protected $action = "updated";
}