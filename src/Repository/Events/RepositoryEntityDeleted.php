<?php
namespace Sourcefragment\LaravelRepository\Events;

/**
 * Class RepositoryEntityDeleted
 *
 * @package Sourcefragment\LaravelRepository\Event
 * @author Krutarth Patel <krutarth@sourcefragment.com>
 */
class RepositoryEntityDeleted extends RepositoryEventBase
{
    /**
     * @var string
     */
    protected $action = "deleted";
}