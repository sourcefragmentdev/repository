<?php
namespace Sourcefragment\LaravelRepository\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class LumenRepositoryServiceProvider
 * @package Sourcefragment\LaravelRepository\Providers
 * @author Krutarth Patel <krutarth@sourcefragment.com>
 */
class LumenRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->commands('Sourcefragment\LaravelRepository\Generators\Commands\RepositoryCommand');
        $this->app->register('Sourcefragment\LaravelRepository\Providers\EventServiceProvider');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
