<?php

namespace Sourcefragment\LaravelRepository\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class RepositoryServiceProvider
 * 
 * @package Sourcefragment\LaravelRepository\Providers
 */

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of provider is deferred
     * @var bool
     */
    protected $defer = false;

    public function boot()
    {
        // Locate package config file
        $this->publishes([ __DIR__ . '/../../../config/repository.php' => config_path('repository.php')]);

        // Copy package config file to application config directory
        $this->mergeConfigFrom(__DIR__ . '/../../../config/repository.php', 'repository');

        $this->loadTranslationsFrom(__DIR__ .'/../../../resources/lang', 'repository');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->commands('Sourcefragment\LaravelRepository\Generators\Commands\RepositoryCommand');
        $this->commands('Sourcefragment\LaravelRepository\Generators\Commands\TransformerCommand');
        $this->commands('Sourcefragment\LaravelRepository\Generators\Commands\PresenterCommand');
        $this->commands('Sourcefragment\LaravelRepository\Generators\Commands\EntityCommand');
        $this->commands('Sourcefragment\LaravelRepository\Generators\Commands\ValidatorCommand');
        $this->commands('Sourcefragment\LaravelRepository\Generators\Commands\ControllerCommand');
        $this->commands('Sourcefragment\LaravelRepository\Generators\Commands\BindingsCommand');
        $this->commands('Sourcefragment\LaravelRepository\Generators\Commands\CriteriaCommand');
        $this->app->register('Sourcefragment\LaravelRepository\Providers\EventServiceProvider');
    }

    /**
     * Get the services provided by the provider
     *
     * @return array|void
     */
    public function provides()
    {
        return [];
    }
}