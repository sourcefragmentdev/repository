<?php
namespace Sourcefragment\LaravelRepository\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class EventServiceProvider
 * @package Sourcefragment\LaravelRepository\Providers
 * @author Krutarth Patel <krutarth@sourcefragment.com>
 */
class EventServiceProvider extends ServiceProvider
{

    /**
     * The event handler mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Sourcefragment\LaravelRepository\Events\RepositoryEntityCreated' => [
            'Sourcefragment\LaravelRepository\Listeners\CleanCacheRepository'
        ],
        'Sourcefragment\LaravelRepository\Events\RepositoryEntityUpdated' => [
            'Sourcefragment\LaravelRepository\Listeners\CleanCacheRepository'
        ],
        'Sourcefragment\LaravelRepository\Events\RepositoryEntityDeleted' => [
            'Sourcefragment\LaravelRepository\Listeners\CleanCacheRepository'
        ]
    ];

    /**
     * Register the application's event listeners.
     *
     * @return void
     */
    public function boot()
    {
        $events = app('events');

        foreach ($this->listen as $event => $listeners) {
            foreach ($listeners as $listener) {
                $events->listen($event, $listener);
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function register()
    {
        //
    }

    /**
     * Get the events and handlers.
     *
     * @return array
     */
    public function listens()
    {
        return $this->listen;
    }
}
